class RegPage < SitePrism::Page
  set_url 'http://192.168.1.89'

  element :register_btn, :xpath, '//*[@id="account"]/ul/li[2]/a'

  element :login_field, '#user_login'
  element :password_field, '#user_password'
  element :confirmation_field, '#user_password_confirmation'
  element :fist_name_field, '#user_firstname'
  element :last_name_field, '#user_lastname'
  element :email_field, '#user_mail'
  element :chkbox_hide_email, '#pref_hide_mail'
  element :language_select, '//*[@id="user_language"]'
  element :submit_button, :xpath, '//*[@id="new_user"]/input[3]'
end
