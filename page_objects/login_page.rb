class LoginPage < SitePrism::Page
  set_url 'http://192.168.1.89'

  element :sign_in_btn, :xpath, '//*[@id="account"]/ul/li[1]/a'

  element :login_field, '#username'
  element :password_field, '#password'
  element :login_button, :xpath, '//*[@id="login-submit"]'
end