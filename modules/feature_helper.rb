module FeatureHelper
  def login_user(username)
    @login_page = LoginPage.new
    @LoginPage.load

    @LoginPage.sign_in_btn.click

    @LoginPage.login_field.set username
    @LoginPage.password_field.set 'gXKGTIZdU0kf'
    @LoginPage.login_button.click
  end

  def reg_user()
    @RegPage = RegPage.new
    @RegPage.load

    @RegPage.register_btn.click

    @RegPage.login_field.set username
    @RegPage.password_field.set password
    @RegPage.confirmation_field.set confirm_password
    @RegPage.fist_name_field.set fist_name
    @RegPage.last_name_field.set last_name
    @RegPage.email_field.set email
    @RegPage.chkbox_hide_email.set(true)
    @RegPage.select 'English', from: 'user_language'
    @RegPage.submit_button.click
  end
end